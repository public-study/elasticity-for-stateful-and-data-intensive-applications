#!/bin/sh

sudo ip route add 224.0.0.0/4 dev eth1

sed "s/CLUSTER_IP/`ip addr show | grep -Eo "192\.168\.50\.1[0-9]"`/" /home/vagrant/activemq/etc/broker.origin.xml > /home/vagrant/activemq/etc/broker.xml && \
sudo /home/vagrant/activemq/bin/artemis run
