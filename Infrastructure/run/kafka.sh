#!/bin/sh

CLUSTER_IP="`ip addr show | grep -Eo "192\.168\.50\.1[0-9]"`"
UNIQUE_ID="`echo $CLUSTER_IP | grep -Eo "[0-9]$"`"

sed "s/CLUSTER_IP/${CLUSTER_IP}/" /home/vagrant/kafka/config/server.origin.properties | \
sed "s/UNIQUE_ID/${UNIQUE_ID}/" > /home/vagrant/kafka/config/server.properties && \
sudo /home/vagrant/kafka/bin/kafka-server-start.sh /home/vagrant/kafka/config/server.properties
