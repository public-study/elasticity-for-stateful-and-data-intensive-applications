package client;

import com.rabbitmq.jms.client.message.RMQTextMessage;
import org.apache.activemq.artemis.jms.client.ActiveMQMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;


@Component
public class Consumer {

    JmsTemplate jmsTemplate;

    @Autowired
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void receive() {
        Message message = jmsTemplate.receive("TestQueue");

        try {
            if (message instanceof RMQTextMessage) {
                System.out.println(((RMQTextMessage) message).getText());
            } else if (message instanceof ActiveMQMessage) {
                System.out.println(message.getBody(String.class));
            } else {
                throw new JMSException("Unknown message type.");
            }

        } catch (JMSException e) {
            System.err.println("Failed to read message body. " + e.getMessage());
        }
    }
}
