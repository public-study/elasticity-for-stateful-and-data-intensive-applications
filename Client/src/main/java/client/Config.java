package client;

import com.rabbitmq.jms.admin.RMQConnectionFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;


@Configuration
public class Config {

    @Value("${broker}")
    String broker;

    @Value("${mom}")
    String mom;

    @Bean
    public ConnectionFactory rabbitMqConnectionFactory() {
        RMQConnectionFactory connectionFactory = new RMQConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost(broker);
        connectionFactory.setPort(5672);
        return connectionFactory;
    }

    @Bean
    public ConnectionFactory activeMqConnectionFactory() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();

        try {
            connectionFactory.setBrokerURL("tcp://" + broker + ":61616");
            connectionFactory.setReconnectAttempts(-1);

            connectionFactory.setConnectionLoadBalancingPolicyClassName(
                    "org.apache.activemq.artemis.api.core.client.loadbalance.RandomConnectionLoadBalancingPolicy"
            );
        } catch (JMSException e) {
            e.printStackTrace();
            System.exit(1);
        }

        try {
            System.out.println(connectionFactory.toURI());
        } catch (Exception e) {
        }

        return connectionFactory;
    }

    @Bean
    public JmsTemplate jmsTemplate() {
        JmsTemplate template = new JmsTemplate();
        if ("R".equalsIgnoreCase(mom)) {
            System.out.println("RABBITMQ");
            template.setConnectionFactory(rabbitMqConnectionFactory());
        } else {
            System.out.println("ACTIVEMQ");
            template.setConnectionFactory(activeMqConnectionFactory());
        }
        return template;
    }
}
