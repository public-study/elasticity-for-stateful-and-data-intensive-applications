package client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.task.SimpleAsyncTaskExecutor;


@SpringBootApplication
public class App implements CommandLineRunner {

    @Autowired
    Producer producer;

    @Autowired
    Consumer consumer;

    @Value("${mode}")
    String mode;

    @Value("${consume-limit}")
    Integer consumeLimit;

    @Value("${concurrency}")
    Integer concurrentThreads;


    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Override
    public void run(String... args) {

        if ("P".equalsIgnoreCase(mode)) {

            System.err.println("PRODUCER");

            for (int i = 0; i < concurrentThreads; i++) {
                System.out.println("Start executor " + i);

                new SimpleAsyncTaskExecutor().execute(() -> {
                    while (true) {
                        try {
                            producer.send();
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                });
            }
        } else {

            System.err.println("CONSUMER");

            for (int i = 0; i < concurrentThreads; i++) {
                System.out.println("Start executor " + i);

                new SimpleAsyncTaskExecutor().execute(() -> {
                    while (true) {
                        try {
                            consumer.receive();

                            if (consumeLimit > 0) {
                                Thread.sleep(1000 / consumeLimit);
                            }

                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                });
            }
        }
    }
}
