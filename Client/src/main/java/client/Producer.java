package client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;


@Component
public class Producer {

    JmsTemplate jmsTemplate;

    long counter = 0;

    @Autowired
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void send() {
        String msg = "MSG:\t " + Thread.currentThread().getId() + "\t " + counter++;
        jmsTemplate.convertAndSend("TestQueue", msg);
        System.out.println(msg);
    }
}
